import Vue from 'vue'
import App from './App'
import store from './store' // store
import plugins from './plugins' // plugins
import './permission' // permission
Vue.use(plugins)
// 引入全局 uview 框架
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)
//字典
import { getDicts } from "@/api/system/dict/data";

Vue.config.productionTip = false
Vue.prototype.$store = store
Vue.prototype.getDicts = getDicts

App.mpType = 'app'

const app = new Vue({
  ...App
})

app.$mount()
