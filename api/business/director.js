import request from '@/utils/request'

// 查询控制器管理列表
export function listDirector(query) {
  return request({
    url: '/business/director/list',
    method: 'get',
    params: query
  })
}

// 查询控制器管理详细
export function getDirector(directorId) {
  return request({
    url: '/business/director/' + directorId,
    method: 'get'
  })
}

// 新增控制器管理
export function addDirector(data) {
  return request({
    url: '/business/director',
    method: 'post',
    data: data
  })
}

// 修改控制器管理
export function updateDirector(data) {
  return request({
    url: '/business/director',
    method: 'put',
    data: data
  })
}

// 删除控制器管理
export function delDirector(directorId) {
  return request({
    url: '/business/director/' + directorId,
    method: 'delete'
  })
}

// 控制器列表
export function listSelect() {
  return request({
    url: '/business/director/listSelect',
    method: 'get'
  })
}
